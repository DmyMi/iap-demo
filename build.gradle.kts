import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.5.7"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.6.0"
	kotlin("plugin.spring") version "1.6.0"
	id("com.google.cloud.tools.appengine-appyaml") version "2.4.2"
}

group = "com.example.gcp"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}
extra["springCloudVersion"] = "Hoxton.SR12"

dependencyManagement {
	imports {
		mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
	}
}

dependencies {
	implementation("org.springframework.cloud:spring-cloud-gcp-starter-security-iap")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

appengine {  // App Engine tasks configuration
	tools {
		cloudSdkVersion = "337.0.0"
	}
	stage {
		setArtifact("${buildDir}/libs/${project.name}-${project.version}.jar")
	}
	deploy {   // deploy configuration
		projectId = "GCLOUD_CONFIG" // gcloud config set project
		version = "GCLOUD_CONFIG"   // gcloud to generate a version
		stopPreviousVersion = true
		promote = true
	}
}
