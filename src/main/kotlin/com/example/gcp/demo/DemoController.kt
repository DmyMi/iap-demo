@file:JvmName("DemoController")

package com.example.gcp.demo

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import java.util.*
import javax.servlet.http.HttpServletRequest


@Controller
class DemoController {

    @GetMapping
    fun index(
    ): String {
        return "index"
    }

    @GetMapping("/secret")
    fun secret(model: Model): String {
        val authentication = SecurityContextHolder.getContext().authentication
        val token = authentication.principal
        if (token is Jwt) {
            with(token) {
                model.addAttribute("user", subject)
                model.addAttribute("email", getClaimAsString("email"))
            }
            return "secret"
        }
        return "403"
    }

    @GetMapping("/headers")
    fun headers(req: HttpServletRequest, model: Model): String {
        val headers = req.headerNames
                .asSequence()
                .filter {
                    it.startsWith("X-Goog", ignoreCase = true) or
                            it.startsWith("X-Appengine", ignoreCase = true)
                }
                .filterNot {
                    it.lowercase(Locale.getDefault()) == "x-appengine-user-ip"
                }
                .map {
                    it to req.getHeader(it)
                }
                .toMap()
        model.addAttribute("headers", headers)
        return "headers"
    }
}