package com.example.gcp.demo

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint

@Configuration
@EnableWebSecurity
class IAPConfiguration : WebSecurityConfigurerAdapter() {
    override fun configure(http: HttpSecurity) {
        http
            .authorizeRequests().antMatchers("/secret").authenticated()
            .and()
            .oauth2ResourceServer().jwt()
            .and()
            .authenticationEntryPoint(Http403ForbiddenEntryPoint())
    }
}