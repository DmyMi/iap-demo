#!/usr/bin/env bash
# Setup OAuth Consent screen at
# https://console.cloud.google.com/apis/credentials/consent
# add test user (a valid gmail you have access to)
# after that go to https://console.cloud.google.com/security/iap
# enable IAP for App Engine switch
# finally add user binding and deploy with this script
# replace USER@gmail.com with the same gmail you used as a test user.
gcloud iap web add-iam-policy-binding \
  --resource-type=app-engine \
  --service=iap-demo \
  --member=user:USER@gmail.com \
  --role=roles/iap.httpsResourceAccessor
gcloud services enable cloudresourcemanager.googleapis.com
./gradlew bootJar appengineDeploy

# to remove the binding run
#gcloud iap web remove-iam-policy-binding \
#  --resource-type=app-engine \
#  --service=iap-demo \
#  --member=user:USER@gmail.com \
#  --role=roles/iap.httpsResourceAccessor
# disable IAP
# gcloud iap web disable --resource-type=app-engine
# don't forget to STOP the app engine service, or it will drain $ :)